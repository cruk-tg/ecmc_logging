require 'serverspec'

set :backend, :exec

describe docker_image('fluent/fluentd:ubuntu-base') do
  it { should exist }
end

describe docker_container('fluent') do
  its(:HostConfig_NetworkMode) { should eq 'docker_net'}
  it { should be_running }
  it { should have_volume('fluentd', '/fluentd/log')}
end
