#
# Cookbook Name:: ecmc_logging
# Recipe:: default
#
# Copyright 2016, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
docker_image 'fluent/fluentd' do
  tag 'ubuntu-base'
end

docker_volume 'fluentd'

docker_container 'fluent' do
  repo 'fluent/fluentd'
  tag 'ubuntu-base'
  volumes ['fluentd:/fluentd/log']
  network_mode 'docker_net'
  restart_policy 'always'
end
